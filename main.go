package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var DBCON *sql.DB

func main() {

	connectDatabase()

	// Handle requests
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("views/index.html")
		if err != nil {
			log.Println(err)
		}

		// Get products from database
		var products = getProductsFromDB()

		// Execute template with data
		err = tmpl.Execute(w, struct{ Products []Product }{products})
		if err != nil {
			log.Println(err)
		}
	})

	http.HandleFunc("/product", func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query().Get("id")
		track := r.URL.Query().Get("track")

		product, err := getProductById(id)
		var productResponse ProductResponse
		productResponse.ProductName = product.ProductName
		productResponse.Description = product.Description
		productResponse.Image = product.Image
		productResponse.Link = product.Link
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		// Jika terdapat parameter "track" dalam URL
		if track != "" {
			productResponse.Show = true

		} else {
			productResponse.Show = false
		}

		// Eksekusi template
		tmpl, err := template.ParseFiles("views/product.html")
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		err = tmpl.Execute(w, productResponse)
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
	})

	http.HandleFunc("/view", func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query().Get("id")
		product, err := getProductById(id)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		tmpl, err := template.ParseFiles("views/view.html")
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		err = tmpl.Execute(w, product)
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
	})

	http.HandleFunc("/get-products", func(w http.ResponseWriter, r *http.Request) {
		products := getProductsFromDB()
		json.NewEncoder(w).Encode(products)
	})

	// Serve static files
	fs := http.FileServer(http.Dir("views/assets"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))

	// Start server
	log.Println("Server started at http://localhost:7812")
	log.Println(http.ListenAndServe(":7812", nil))
}

func redirectToView(w http.ResponseWriter, r *http.Request, product ProductResponse, id string) {
	http.Redirect(w, r, fmt.Sprintf("/view?id=%s", id), http.StatusSeeOther)
}

const (
	host = "vmn.h.filess.io"

	port = "3307"

	user = "kitashopdb_teaslopewe"

	password = "ee09f6b4565cc5bb765eef9bfa87864a0ed980ca"

	dbname = "kitashopdb_teaslopewe"
)

type Product struct {
	ID          string    `json:"id"`
	ProductName string    `json:"product_name"`
	Description string    `json:"description"`
	Image       string    `json:"image"`
	Link        string    `json:"link"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type ProductResponse struct {
	ID          string `json:"id"`
	ProductName string `json:"product_name"`
	Description string `json:"description"`
	Image       string `json:"image"`
	Link        string `json:"link"`
	Show        bool   `json:"show"`
	URL         string `json:"url"`
}

func connectDatabase() {
	uri := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s", user, password, host, port, dbname)
	db, err := sql.Open(
		"mysql", uri)

	if err != nil {
		log.Println(err)
	}

	var version string
	err2 := db.QueryRow("SELECT VERSION()").Scan(&version)
	if err2 != nil {
		log.Println(err2)
	}

	fmt.Println(version)
	DBCON = db
}

func getProductById(id string) (Product, error) {
	var product Product
	query := "SELECT product_name, image, description, link FROM products WHERE id = ?"
	err := DBCON.QueryRow(query, id).Scan(&product.ProductName, &product.Image, &product.Description, &product.Link)
	if err != nil {
		if err == sql.ErrNoRows {
			return product, errors.New("product not found")
		}
		log.Println(err)
	}

	return product, nil
}

func getProductsFromDB() []Product {
	// Query to fetch products from database
	rows, err := DBCON.Query("SELECT * FROM products")
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	// Retrieve data from query result
	var products []Product
	for rows.Next() {
		var product Product
		// Declare variables to scan the string values from the database
		var createdAtStr, updatedAtStr string
		err := rows.Scan(&product.ID, &product.ProductName, &product.Image, &product.Link, &createdAtStr, &updatedAtStr, &product.Description)
		if err != nil {
			log.Println(err)
		}
		// Parse createdAtStr and updatedAtStr to time.Time
		createdAt, err := time.Parse("2006-01-02 15:04:05", createdAtStr)
		if err != nil {
			log.Println(err)
		}
		updatedAt, err := time.Parse("2006-01-02 15:04:05", updatedAtStr)
		if err != nil {
			log.Println(err)
		}
		// Assign parsed time values to product struct
		product.CreatedAt = createdAt
		product.UpdatedAt = updatedAt
		// Append product to products slice
		products = append(products, product)
	}
	if err := rows.Err(); err != nil {
		log.Println(err)
	}

	return products
}
